AAAAAA

25104-12042010
00000WmzoZUj7MZySlNk"L5dIQ1T5e
Lxq9DDfvxFe0!xPhzcQknfYK6R0SOO
CMIo9v3QdQ7UlPNHxrKqCvNKrMsm6V








====================================================================
== Platform branch                                                ==
====================================================================
Phase-1
    [+STRAT] Martingale
    Parallel optimization
    Compare backtest results with MT4
    If good than pinpoint results with unit tests
Phase-2
    [+STRAT] A simple pair-trading strat
    [+STRAT] Machine learning strat
Phase-3
    Paper trading __some__ strategy on dukas
    More unit tests
    Integration tests
Phase-4
    Golive on dukas



====================================================================
== Profile branch                                                 ==
====================================================================
    - Dukas account
    - Dukas article contest
    - Dukas strategy contest 5.000



====================================================================
== TODO                                                           ==
====================================================================
    - move to bitbucket





Phase 1
    make sure backtester is correct
    parallel optimization

Phase 2
    dukas run live
    live monitoring / phone
    discrepancy between bt and live / stats


Phase 3
    build a winner strat
        sckikit learn
        pair trade
        pybrain , ANN
        breakout strat

Phase 4
    needs unit test with given data
    test with random walks











#indicator
   * indicator prediction strength number
   * linear combination of indicators
   * pyTaLib

#chart
   * scrolling
   * zooming
   * legend, data navigator

#backtest
   * tick test
   * convert backtester to module|class
   * [optimize] filter out ticks from dataset below certain treshold
   * parallelize
   * bt continuation (pickling process and inputs)
   * interruptible (control-c)



#lockpick
   * build strat
   * compare with mt4 version v38




#bridge
   * zero mq
   * named pipes




#ml
   * scikit
   * statmodels
   * pybrain


#pair-trade
   * HFT pair trade strategy


#performance metrics
   * sharpe ratio
   * sortino ratio
   * drawdown
   * profit ratio
