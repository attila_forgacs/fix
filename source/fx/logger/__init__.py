# coding=utf-8
# http://docs.python.org/2/howto/logging-cookbook.html#logging-cookbook

import sys
import logging
import logging.config

LOG_FILE = '/home/attila/Desktop/fx_normal.log'

dictLogConfig = {
    "version": 1,
    "handlers": {
        "consoleHandler": {
            "class": "logging.StreamHandler",
            "formatter": "normalFormatter",
            "level": "DEBUG",
            "stream": sys.stdout,
        },
        "fileHandler": {
            "class": "logging.FileHandler",
            "formatter": "normalFormatter",
            "filename": LOG_FILE
        }
    },
    "loggers": {
        "default": {
            "handlers": ["consoleHandler", 'fileHandler'],
            "level": "DEBUG",
        }
    },

    "formatters": {
        "normalFormatter": {
            #"format": "%(asctime)s - %(name)s - %(levelname)6s ::: %(message)s"
            #"format": "%(levelname)-4s • %(message)s"
            #"format": "%(levelname).2s • %(message)s"
            "format": "%(levelname).1s• %(message)s"
        }
    }
}

logging.config.dictConfig(dictLogConfig)

fx_log = logging.getLogger("default")
fx_log.setLevel(logging.DEBUG)
