class Bar(object):
    def __init__(self, open, high, low, close, volume, frequency=None):
        super(Bar, self).__init__()
        self.open = open
        self.high = high
        self.low = low
        self.close = close
        self.volume = volume
        self.frequency = frequency

    def tostr(self):
        return 'Bar[O:%f H:%f L:%f C:%f V:%f]' % (
            self.open,
            self.high,
            self.low,
            self.close,
            self.volume,
        )

    __unicode__ = tostr
    __str__ = tostr
    __repr__ = tostr
