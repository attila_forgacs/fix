from fx.tools.smartobject import SmartObject


class Instrument(SmartObject):
    """
    Singleton for each tradable instrument
    """

    def __init__(self, name="", one_pips_value=0.0000, lot=100000):
        super(Instrument, self).__init__()
        self.__name = name
        self.__one_pips_value = one_pips_value
        self.__lot = lot

    @property
    def name(self):
        return self.__name

    @property
    def pip(self):
        """
        1 pips value
        """
        return self.__one_pips_value

    @property
    def lot(self):
        """
        Lot size for this currency
        Something like 100000
        """
        return self.__lot

    def __str__(self):
        return '{0}'.format(self.name)

    def __hash__(self):
        return hash((self.name, self.pip, self.lot))

    def __eq__(self, other):
        assert other is not None
        return (
            (self.name, self.pip, self.lot)
            ==
            (other.name, other.pip, other.lot)
        )


EURUSD = Instrument(name='EURUSD', one_pips_value=0.0001, lot=10 ** 5)

