import matplotlib

matplotlib.use('WXAgg')
import numpy as np
import pandas as pd
import pandas.stats.moments
import ipdb
import wx
from matplotlib.backends.backend_wxagg import FigureCanvasWxAgg
from matplotlib.figure import Figure
from pylab import *


rc("figure.subplot", left=0.03)
rc("figure.subplot", right=0.99)
rc("figure.subplot", bottom=0.03)
rc("figure.subplot", top=0.98)
rc("figure", fc='EEEEEE')
rc("axes", ec='CCCCCC')
rc("keymap", save='')
rc('grid', color='0.75', linestyle='-', linewidth=0.5)

ds = pd.HDFStore('/home/attila/data/ticks.h5')
ds = ds['t1']
onemin = ds.A.resample('10S', how='ohlc')
O = onemin.open.fillna(method='ffill')
C = onemin.close.fillna(method='ffill')
H = onemin.high.fillna(method='ffill')
L = onemin.low.fillna(method='ffill')
MF = pandas.stats.moments.ewma(C, span=12)
MS = pandas.stats.moments.ewma(C, span=26)
MACD = MF - MS
SIGNAL = pandas.stats.moments.ewma(MACD, span=9)
MACD_HIST = MACD - SIGNAL


def ploo(what, idx=None, **kwargs):
    if not idx:
        w = what
    else:
        w = what[idx[0]:idx[1]]
    w.plot(**kwargs)


def shoo():
    mng = plt.get_current_fig_manager()
    mng.frame.Maximize(True)
    grid(True, linestyle='-', color='0.55')
    plt.show()


def keypress(event):
    INCREMENT = 1.01
    DECREMENT = 0.99
    #    print event.key, "\n", dir(event)
    if event.key in ('s'):
        xmin, xmax = xlim()
        plt.xlim(xmin * INCREMENT, xmax * INCREMENT)
    elif event.key in ('a'):
        xmin, xmax = xlim()
        plt.xlim(xmin * DECREMENT, xmax * DECREMENT)
    elif event.key in (' '):
        quit()
    draw()


def mousepress(event):
    quit()


uptill = 50

ax1 = subplot(211)
ploo(C, (0, uptill))

ax2 = subplot(212)

ploo(MACD, (0, uptill), style="b-", ax=ax2)
ploo(SIGNAL, (0, uptill), style="r-- ", ax=ax2)
ax3 = ax2.twinx()
ax2.bar(MACD_HIST.index[0:uptill], MACD_HIST[0:uptill])

#ploo(MACD_HIST, (0, uptill), style="k-" , ax=ax2)
#MACD_HIST.head(uptill).plot(kind='bar', use_index=False, xticks=[], yticks=[], ax=ax2)

#connect('key_press_event', keypress)
#connect('button_press_event', mousepress)
#shoo()


class MyFrame(wx.Frame):
    def __init__(self, parent, id):
        wx.Frame.__init__(self, parent, id, 'scrollable plot',
                          style=wx.DEFAULT_FRAME_STYLE ^ wx.RESIZE_BORDER,
                          size=(800, 400))
        self.panel = wx.Panel(self, -1)

        self.fig = Figure((5, 4), 75)
        self.canvas = FigureCanvasWxAgg(self.panel, -1, self.fig)
        self.scroll_range = 400
        self.canvas.SetScrollbar(wx.HORIZONTAL, 0, 5,
                                 self.scroll_range)

        sizer = wx.BoxSizer(wx.VERTICAL)
        sizer.Add(self.canvas, -1, wx.EXPAND)

        self.panel.SetSizer(sizer)
        self.panel.Fit()

        self.init_data()
        self.init_plot()

        self.canvas.Bind(wx.EVT_SCROLLWIN, self.OnScrollEvt)

    def init_data(self):
        # Generate some data to plot:
        self.t = MACD.index
        self.x = MACD

        # Extents of data sequence:
        self.i_min = 0
        self.i_max = len(self.t)

        # Size of plot window:
        self.i_window = 100

        # Indices of data interval to be plotted:
        self.i_start = 0
        self.i_end = self.i_start + self.i_window

    def init_plot(self):
        self.axes = self.fig.add_subplot(111)
        self.plot_data = \
            self.axes.plot(self.t[self.i_start:self.i_end],
                           self.x[self.i_start:self.i_end])[0]

    def draw_plot(self):
        # Update data in plot:
        self.plot_data.set_xdata(self.t[self.i_start:self.i_end])
        self.plot_data.set_ydata(self.x[self.i_start:self.i_end])

        # Adjust plot limits:
        self.axes.set_xlim((min(self.t[self.i_start:self.i_end]),
                            max(self.t[self.i_start:self.i_end])))
        self.axes.set_ylim((min(self.x[self.i_start:self.i_end]),
                            max(self.x[self.i_start:self.i_end])))

        # Redraw:
        self.canvas.draw()

    def OnScrollEvt(self, event):
        # Update the indices of the plot:
        self.i_start = self.i_min + event.GetPosition()
        self.i_end = self.i_min + self.i_window + event.GetPosition()
        self.draw_plot()


class MyApp(wx.App):
    def OnInit(self):
        self.frame = MyFrame(parent=None, id=-1)
        self.frame.Show()
        self.SetTopWindow(self.frame)
        return True


if __name__ == '__main__':
    app = MyApp()
    app.MainLoop()
