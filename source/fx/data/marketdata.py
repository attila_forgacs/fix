from collections import defaultdict
from fx.engine.frequency import FREQ_TICK, Frequency
from fx.engine.instrument import Instrument
from fx.logger import fx_log
from fx.data.bar_series import BarSeries


class MarketData(object):
    MAX_QUOTE_STORED = 500

    def __init__(self, instruments=None):
        super(MarketData, self).__init__()
        self._listeners = defaultdict(list)
        self.quotes = {}
        self.bars = {}
        for instrument in instruments or []:
            self.quotes[instrument] = []
            self.bars[instrument] = {}

    def allocate_barseries_storage(self, frequency, instrument):
        if frequency not in self.bars[instrument]:
            self.bars[instrument][frequency] = BarSeries(
                instrument=instrument,
                frequency=frequency,
            )

    def register_listener(self, callback, frequency, instrument):
        self._listeners[(instrument, frequency)].append(callback)

    def subscribe(self, instrument, frequency, callback):
        assert callable(callback)
        assert isinstance(instrument, Instrument)
        assert isinstance(frequency, Frequency)

        fx_log.info(
            'subscribing on {instrument} {frequency}'.format(**locals())
        )

        if not callback in self._listeners[(instrument, frequency)]:
            self.register_listener(callback, frequency, instrument)
            self.allocate_barseries_storage(frequency, instrument)

    def un_subscribe(self, instrument, frequency, callback):
        assert callable(callback)
        fx_log.info(
            'un-subscribed on {instrument} {frequency}'.format(**locals()))
        if callback in self._listeners[(instrument, frequency)]:
            self._listeners[(instrument, frequency)].remove(callback)

    def _notify(self, instrument, frequency, data):
        """
        Notify instrument observers at given frequency
        for data change.
        """
        for callback in self._listeners[(instrument, frequency)]:
            callback(data)


    def add_quote(self, quote, quote_count=0):

        instrument = quote.instrument

        # store/truncate quote
        self.quotes[instrument].append(quote)
        if len(self.quotes[instrument]) >= 2 * self.MAX_QUOTE_STORED:
            self.quotes[instrument] = self.quotes[instrument][
                                      self.MAX_QUOTE_STORED:]

        all_frequencies_to_check = [
            key[1] for key, listeners in
            self._listeners.iteritems() if
            key[0] == instrument and
            key[1] != FREQ_TICK
        ]

        for frequency_to_update in all_frequencies_to_check:


            listeners_to_notify = [
                (key, value) for key, value in
                self._listeners.iteritems() if
                key[0] == instrument and
                key[1] != FREQ_TICK
            ]

            for registration_pair, listeners in listeners_to_notify:

                this_instrument, this_frequency = registration_pair

                if this_frequency == frequency_to_update:

                    barseries = self.bars[instrument][frequency_to_update]
                    is_new_bar_created = barseries.append(quote)

                    if is_new_bar_created:
                        for listener in listeners:
                            listener(barseries)