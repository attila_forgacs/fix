#!/usr/bin/env python
#http://docs.python.org/2/howto/argparse.html#id1
import pandas as pd
import argparse
import struct

parser = argparse.ArgumentParser()
parser.add_argument("infile", help="dukas csv to convert")
parser.add_argument("outfile", help="binary to output")
parser.add_argument("--chunksize", help="chunk size", type=int, default=100000)
args = parser.parse_args()

chunk_size = args.chunksize
infile = args.infile
outfile = args.outfile

print "Converting from:"
print infile
print "to:"
print outfile
print "chunksize:%s" % (chunk_size)
print

dataframe = pd.read_csv(
    infile,
    index_col=0,
    parse_dates=[0],
    names=['TIME', 'A', 'B', 'ASKV', 'BIDV'],
    nrows=None,
    skiprows=1,
    iterator=True,
    chunksize=chunk_size
)

chunks_count = 0

with open(outfile, 'wb') as filehandle:
    for chunk in dataframe:
        for row in chunk.itertuples():
            # date as pandas Timestamp
            date = row[0].value
            ask = row[1]
            bid = row[2]
            askv = row[3]
            bidv = row[4]
            buff = struct.pack('qffff', date, ask, bid, askv, bidv)
            filehandle.write(buff)
        chunks_count += chunk_size
        print 'done with:%s' % chunks_count

if "TEST" == 0:
    with open(outfile, 'rb') as filehandle:
        c = filehandle.read()
        dat = struct.unpack('qffff' * 100, c)
        print pd.Timestamp(dat[0])

print
print "Conversion ready of "
print dataframe
print "rows..."