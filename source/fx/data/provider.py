import struct
import pandas as pd
import numpy as np
from fx.backtest.settings import BackTestSettings
from fx.engine.instrument import EURUSD
from fx.engine.quote import Quote


def _random_generator(range_min, range_max, pip=0.0001):
    QUOTE_COUNT = 2500
    SPREAD = np.random.randint(0, 25,
                               size=QUOTE_COUNT) * (pip / 10.0)
    BASE = 1.2000
    ds = pd.DataFrame(columns=['A', 'B', 'ASKV', 'BIDV'],
                      index=pd.date_range('1/1/2013', freq='1S',
                                          periods=QUOTE_COUNT, ),
    )
    ds.A = (
               np.random.randint(range_min, range_max,
                                 size=QUOTE_COUNT)) * pip
    ds.A[0] = BASE
    ds.A = ds.A.cumsum()
    ds.B = ds.A - SPREAD
    ds.ASKV = np.random.randint(0, 1000, size=QUOTE_COUNT)
    ds.BIDV = np.random.randint(0, 1000, size=QUOTE_COUNT)
    tick_iterator = ds.iterrows()
    for row in tick_iterator:
        quote = Quote.from_tuple(row, instrument=EURUSD)
        yield quote


def _sine_generator(pip=0.0001):
    QUOTE_COUNT = 2500
    SPREAD = np.random.randint(0, 25,
                               size=QUOTE_COUNT) * (pip / 10.0)
    BASE = 1.2000
    ds = pd.DataFrame(columns=['A', 'B', 'ASKV', 'BIDV'],
                      index=pd.date_range('1/1/2013', freq='1S',
                                          periods=QUOTE_COUNT, ),
    )
    ds.A = BASE + np.sin(np.arange(0, QUOTE_COUNT, 1)) * 0.0001 * 20
    ds.B = ds.A - SPREAD
    ds.ASKV = np.random.randint(0, 1000, size=QUOTE_COUNT)
    ds.BIDV = np.random.randint(0, 1000, size=QUOTE_COUNT)
    tick_iterator = ds.iterrows()
    # TODO yield quotes
    return tick_iterator


def binary_reader():
    struct_format = 'qffff' # date=q, 4 floats
    struct_size = struct.calcsize(struct_format) # 24 for 'qffff'
    pieces_in_chunk = 1000 # do 1000 items in a run
    source_file = r'/mnt/ssd120/data/eurusd_2012.bin'
    #source_file = r'/ram/eurusd_2012.bin'
    with open(source_file,
              'rb') as file_handle:
        while True:
            chunk = file_handle.read(pieces_in_chunk * struct_size)
            if not chunk:
                return
            quotes_actually_read = len(chunk) / struct_size
            huge_concatenated_tuple = struct.unpack(
                struct_format * quotes_actually_read,
                chunk)
            for i in range(quotes_actually_read):
                offset = i * 5
                timestamp = pd.Timestamp(huge_concatenated_tuple[offset + 0])
                ask = huge_concatenated_tuple[offset + 1]
                bid = huge_concatenated_tuple[offset + 2]
                askv = huge_concatenated_tuple[offset + 3]
                bidv = huge_concatenated_tuple[offset + 4]
                q = Quote(
                    timestamp=timestamp,
                    instrument=EURUSD, # TODO hardcoded
                    bid=bid,
                    ask=ask,
                    bidvol=bidv,
                    askvol=askv)
                yield q


def get_data_iterator(backtest_settings):
    dt = backtest_settings.data_type

    if dt == BackTestSettings.DATA_TYPE_HDF5:
        ds = pd.HDFStore('/mnt/ssd120/data/ticks.h5', mode='r')
        tick_iterator = ds['EURUSD'].iterrows() # TODO hardcoded
        return tick_iterator

    if dt == BackTestSettings.DATA_TYPE_RANDOM:
        return _random_generator(-4, 4)

    if dt == BackTestSettings.DATA_TYPE_ALWAYS_UP:
        return _random_generator(0, 8)

    if dt == BackTestSettings.DATA_TYPE_ALWAYS_DOWN:
        return _random_generator(-8, -1)

    if dt == BackTestSettings.DATA_TYPE_ALWAYS_FLAT:
        return _random_generator(-1, 1, pip=0)

    if dt == BackTestSettings.DATA_TYPE_ALWAYS_SINE:
        return _sine_generator()

    if dt == BackTestSettings.DATA_TYPE_BINARY:
        return binary_reader()

