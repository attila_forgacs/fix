class FXEngineException(Exception):
    """
    Generic error signalling internal coding and logical issues
    that is not related to market/broker, etc...
    """
    pass


class FXMarginCallException(FXEngineException):
    """
    Broker issues margin call
    """
    pass

