from datetime import timedelta
from fx.engine.marketside import MarketSide
from fx.engine.quote import Quote


class Position(object):
    """
    Semi-immutable object representing a position
    taken on an instrument for a given side, with
    n lots.

    Contains information on stops as well.

    SL and TP are stored in dimensionless cardinals (e.g. 40.0),
    floats so pip-decimals are allowed.

    Terminology
        price : 1.2000
        sl/tp : 0.1 or 10 or 15.1
        strategy-id : string like "my-macross-2013/2"
        ticket : brokers internal id representing this position

    """

    def __init__(
            self,
            instrument=None,
            side=None,
            lot=0,
            quote_opened=None,
            strategy_id=None,
            ticket=None,
            margin_cost=None,
    ):

        super(Position, self).__init__()
        assert isinstance(quote_opened, Quote)

        self.instrument = instrument
        self.strategy_id = strategy_id
        self.ticket = ticket

        self.is_open = True
        self.q1 = quote_opened
        self.q2 = None

        self.lot = lot
        self.margin_cost = margin_cost
        self.side = side
        self.long = side == MarketSide.LONG
        self.short = side == MarketSide.SHORT

        self.profit_in_pips = 0
        self.profit_in_base_currency = 0

        self.tp = None
        self.sl = None

    def set_tp(self, tp):
        self.tp = tp
        return self

    def set_tp_price(self, tp_price):
        if self.long:
            self.set_tp((tp_price - self.q1.ask) / self.instrument.pip)
        else:
            self.set_tp((self.q1.bid - tp_price) / self.instrument.pip)
        return self

    def set_sl(self, sl):
        self.sl = sl
        return self

    def set_sl_price(self, sl_price):
        if self.long:
            self.set_sl((self.q1.ask - sl_price) / self.instrument.pip)
        else:
            self.set_sl((sl_price - self.q1.bid) / self.instrument.pip)
        return self

    def set_stops(self, sl=0.0, tp=0.0):
        """
        Convenience function for setting both sl/tp
        """
        self.set_sl(sl)
        self.set_tp(tp)
        return self

    @property
    def tp_price(self):
        """
        Take profit price
        """
        assert isinstance(self.tp, (int, float))
        assert isinstance(self.q1, Quote)

        if self.long:
            price = self.q1.ask
            direction = +1
        else:
            price = self.q1.bid
            direction = -1

        return price + direction * (self.tp * self.instrument.pip)

    @property
    def sl_price(self):
        """
        Stop loss price
        """
        assert isinstance(self.sl, (int, float))
        assert isinstance(self.q1, Quote)

        if self.long:
            price = self.q1.ask
            direction = -1
        else:
            price = self.q1.bid
            direction = +1

        return price + direction * (self.sl * self.instrument.pip)

    def update_state(self, last_quote):
        """
        update profit pip and money variables
        based on last quote and this quote
        """
        assert isinstance(last_quote, Quote)
        assert isinstance(self.q1, Quote)
        assert self.is_open
        if self.long:
            self.profit_in_pips = last_quote.bid - self.q1.ask
        else:
            self.profit_in_pips = self.q1.bid - last_quote.ask

        # TODO profit only valid for USD base, have to convert if base differs
        self.profit_in_base_currency = (
            self.profit_in_pips
            * self.lot
            * self.instrument.lot
        )
        assert isinstance(self.profit_in_base_currency, (float, int))

    def mark_to_market(self, last_quote):
        assert self.is_open
        self.update_state(last_quote)
        self.q2 = last_quote
        return self

    def close(self):
        self.is_open = False
        return self

    @property
    def lifetime(self):
        if not self.is_open:
            at_close = self.q2
            at_open = self.q1
            time_diff = at_close.timestamp - at_open.timestamp
            return time_diff
        else:
            return timedelta(seconds=0)

    def tostring(self):
        try:
            close_price = (self.q2.ask if self.side == MarketSide.SHORT
                           else self.q2.bid)
        except AttributeError:
            close_price = -1
        assert isinstance(self.lifetime, timedelta)
        lifetime_min = self.lifetime.seconds / 60.0
        return (
            ("{dir:5s} {lots:3.2f} pips {pip_profit: 8.5f} " +
             "$ {pip_money: 8.2f} open {price_open: 8.5f} close {" +
             "price_close: 8.5f} {lifetime:8.2f} min").format(
                dir='long' if self.side == MarketSide.LONG else 'short',
                lots=self.lot,
                pip_profit=self.profit_in_pips,
                pip_money=self.profit_in_base_currency,
                price_open=(self.q1.bid
                            if self.side == MarketSide.SHORT
                            else self.q1.ask),
                price_close=close_price,
                lifetime=lifetime_min,
            )
        )

    __repr__ = tostring
    __str__ = tostring

    @property
    def has_stops(self):
        return self.has_tp or self.has_sl

    @property
    def has_tp(self):
        return self.tp is not None

    @property
    def has_sl(self):
        return self.sl is not None

