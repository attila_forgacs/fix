import pprint
import copy
from fx.conf.constants import (COLUMN_BID, COLUMN_ASK, COLUMN_ASK_VOLUME,
                               COLUMN_BID_VOLUME)


class Quote(object):
    def __init__(self, timestamp=None, instrument=None, bid=None, ask=None,
                 bidvol=None, askvol=None):
        """
        Quote object
        book - not used yet
        """
        super(Quote, self).__init__()
        self.timestamp = timestamp
        self.instrument = instrument
        self.bid = bid
        self.ask = ask
        self.askvol = askvol
        self.bidvol = bidvol
        #assert isinstance(bid, float)
        #assert isinstance(ask, float)
        #assert isinstance(askvol, float)
        #assert isinstance(bidvol, float)

    @property
    def total_volume(self):
        return self.askvol + self.bidvol

    def __repr__(self):
        return pprint.pformat(vars(self))

    def __str__(self):
        return '%s[%.4f/%.4f]' % (self.instrument, self.bid, self.ask)

    def as_dict(self):
        """
        Returns this quote as a dictionary.
        @return:
            dictionary representation of quote.
        """
        return {
            'timestamp': self.timestamp,
            'bid': self.bid,
            'ask': self.ask,
            'bidvol': self.bidvol,
            'askvol': self.askvol
        }

    @staticmethod
    def from_dataframe(df, instrument):
        """
        Converts dataframe 's last item (ask,askvol,bid,bidvol)
        with timestamp index to quote.
        @param df:
            Dataframe
        @return:
            Quote
        """
        row = df.tail(1)
        return Quote(
            timestamp=row.index[0],
            instrument=instrument,
            bid=row.bid[0],
            ask=row.ask[0],
            bidvol=row.bidvol[0],
            askvol=row.askvol[0]
        )

    @staticmethod
    def from_tuple(tuple, instrument):
        return Quote(
            timestamp=tuple[0],
            instrument=instrument,
            bid=tuple[1].get_value(COLUMN_BID),
            ask=tuple[1].get_value(COLUMN_ASK),
            bidvol=tuple[1].get_value(COLUMN_BID_VOLUME),
            askvol=tuple[1].get_value(COLUMN_ASK_VOLUME),
        )

    def clone(self):
        return copy.deepcopy(self)
