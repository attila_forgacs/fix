class Frequency(object):
    '''

    Pandas time constants
    ---------------------

    B	business day frequency
    D	calendar day frequency
    W	weekly frequency
    M	month end frequency
    BM	business month end frequency
    MS	month start frequency
    BMS	business month start frequency
    Q	quarter end frequency
    BQ	business quarter endfrequency
    QS	quarter start frequency
    BQS	business quarter start frequency
    A	year end frequency
    BA	business year end frequency
    AS	year start frequency
    BAS	business year start frequency
    H	hourly frequency
    T	minutely frequency
    S	secondly frequency
    L	milliseonds
    U	microseconds
    '''
    def __init__(self, seconds=0):
        super(Frequency, self).__init__()
        self.__seconds = seconds

    @property
    def seconds(self):
        return self.__seconds

    @property
    def as_pandas_constant(self):
        if self.__seconds >= 60:
            return "%dT" % (self.__seconds / 60.0)
        else:
            return "%dS" % (self.__seconds)

    def __str__(self):
        try:
            return '<frequency:{0}>'.format(self.as_pandas_constant)
        except NotImplementedError:
            return '<frequency:Tick>'

    __repr__ = __str__
    __unicode__ = __str__


class TickFrequency(Frequency):
    def __init__(self, seconds=None):
        super(TickFrequency, self).__init__(seconds)

    @property
    def as_pandas_constant(self):
        raise NotImplementedError()


FREQ_TICK = TickFrequency()
FREQ_1_SEC = Frequency(1)
FREQ_5_SEC = Frequency(5)
FREQ_1_MIN = Frequency(60)
FREQ_2_MIN = Frequency(2 * 60)
FREQ_5_MIN = Frequency(5 * 60)
FREQ_30_MIN = Frequency(30 * 60)
FREQ_1_HOUR = Frequency(60 * 60)
