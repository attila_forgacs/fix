from fx.engine.marketside import MarketSide
from fx.engine.ordertype import OrderType


class AbstractStrategy(object):
    def __init__(self, broker=None, strategy_id=None):
        """
        @type broker: AbstractBroker
        @type strategy_id: str
        """
        self.broker = broker
        self.strategy_id = strategy_id

    def on_bar(self, bar):
        """
        Process observed instruments bar
        @type bar: Bar
        """
        pass

    def on_quote(self, quote):
        """
        @type quote: Quote
        """
        pass

    @property
    def my_positions(self):
        return self.broker.positions.of(self)


LONG = MarketSide.LONG
SHORT = MarketSide.SHORT
MARKET = OrderType.MARKET