import pandas as pd

'''
    Time,Ask,Bid,AskVolume,BidVolume
    2012.12.02 22:00:16.417,1.29837,1.2983,0.75,0.75
'''

ds = pd.HDFStore('/media/attila/ssd120/data/ticks.h5')

d1 = pd.read_csv(
    r'/media/attila/ssd120/data/EURUSD_Ticks_2012.01.01_2012.12.31.csv',
    index_col=0,
    parse_dates=[0],
    names=['TIME', 'A', 'B', 'ASKV', 'BIDV'],
    nrows=None,
    skiprows=1
)

#d1 = d1.resample('5s').dropna()

ds['EURUSD'] = d1
ds.flush()
ds.close()
