import copy
import cPickle
import time
import gc

import pylab as pl
import sys

from fx.backtest.settings import BackTestSettings
from fx.data.provider import get_data_iterator
from fx.engine.frequency import Frequency
from fx.evaluation.evaluator import DummyEvaluator
from fx.strategy.martingale.strategy import MartingaleStrategy
from fx.tools.mem_usage import resident, try_free_mem


class BackTest(object):
    """

    """

    def __init__(self,
                 settings=None,
                 broker=None,
                 strategies=None
    ):

        super(BackTest, self).__init__()

        self.quotes = []
        self.strategies = strategies
        self.broker = broker
        self.settings = settings

    def shutdown(self):
        for strategy in self.strategies:
            self.broker.actions.close_all_position(strategy)

    def test(self, quotes=None, evaluator=DummyEvaluator(), instrument=None):

        assert instrument

        self.quotes = quotes or get_data_iterator(self.settings)
        on_quote = self.broker.on_quote
        record = evaluator.record
        account = self.broker.account
        quote_counter = 0
        last_time = time.clock()
        last_mem = 0
        mems = []
        times = []
        quote_limit = 0 * 1000
        quote_block_size = 100 * 1000
        backtest_start = time.clock()

        account_start = copy.deepcopy(account)

        try:

            print '-' * 80

            for quote in self.quotes:

                quote_counter += 1
                on_quote(quote, quote_counter)

                if quote_counter % 1000 == 0:
                    record(dict(eq=account.equity, bl=account.balance, ))

                if quote_counter % quote_block_size == 0:
                    mem = resident() / (1024 ** 2)
                    mem_diff = mem - last_mem
                    last_mem = mem
                    clock = time.clock()
                    timediff = clock - last_time
                    time_ellapsed_so_far = clock - backtest_start
                    print "mem{0: 3.0f}:{1:5.2f} Q{2:6n}K / T{3: n} {4:5.2f}M".format(
                        mem,
                        mem_diff,
                        quote_counter / 1000,
                        timediff,
                        time_ellapsed_so_far / 60.0,
                    )
                    last_time = time.clock()
                    mems.append(mem)
                    times.append(timediff)
                    gc.collect()
                    try_free_mem()

                if quote_limit and quote_counter >= quote_limit:
                    break

        except KeyboardInterrupt:
            print "Interrupted!!!Control/C"

        self.shutdown()

        account_stop = copy.deepcopy(account)

        backtest_stop = time.clock()
        backtest_time_ellapsed = backtest_stop - backtest_start
        record(dict(eq=account.equity, bl=account.balance, ))

        print '-' * 80
        print 'Total {0:15n} quotes'.format(quote_counter)
        print 'Time: {0:15f} seconds'.format(backtest_time_ellapsed)
        print 'Time: {0:15f} minutes'.format(backtest_time_ellapsed / 60.0)
        print
        print

        profit = account_stop.equity - account_start.equity
        print 'Profit: {0:5.2f}'.format(profit)

        positions = self.broker.positions.all_positions_ever
        print 'Positions: {0}'.format(len(positions))

        cPickle.dump(positions, open('/home/attila/positions.pickle', 'wb'))

        pl.title('Memory')
        pl.plot(mems)
        pl.savefig('/home/attila/Desktop/memory.png')

        pl.title('Time')
        pl.plot(times)
        pl.savefig('/home/attila/Desktop/time.png')


if __name__ == '__main__':
    from fx.broker.backtest.btbr_broker import BackTestBroker
    from fx.backtest.backtest import BackTest
    from fx.engine.instrument import EURUSD
    from fx.evaluation.evaluator import Evaluator
    from fx.strategy.martingale.params import MartingaleParameters

    sys.setcheckinterval(10 ** 9)

    backtest_settings = BackTestSettings(
        data_type=BackTestSettings.DATA_TYPE_BINARY)
    broker = BackTestBroker.create(instruments=[EURUSD],
                                   settings=backtest_settings)

    p = MartingaleParameters()
    p.frequency = Frequency(30 * 60)
    p.instrument = EURUSD
    p.validate()

    martingale_strat = MartingaleStrategy(broker=broker,
                                          parameters=p,
                                          strategy_id='martingale-1')
    evaluator = Evaluator()

    BackTest(
        settings=backtest_settings,
        broker=broker,
        strategies=[martingale_strat]
    ).test(
        evaluator=evaluator,
        instrument=EURUSD
    )

    evaluator.evaluate()


