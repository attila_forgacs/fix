from unittest import TestCase
from fx.engine.instrument import EURUSD
from fx.engine.marketside import MarketSide
from fx.engine.position import Position
from fx.engine.quote import Quote
from fx.engine.errors import FXEngineException


class TestPosition(TestCase):
    def setUp(self):
        super(TestPosition, self).setUp()
        self.dalong = Position(
            instrument=EURUSD,
            side=MarketSide.LONG,
            lot=1,
            quote_opened=Quote(None, EURUSD, 1.2000, 1.2002, 0, 0),
            strategy_id='ma-strat-001',
            ticket='12983719082479521'
        )
        self.dashort = Position(
            instrument=EURUSD,
            side=MarketSide.SHORT,
            lot=1,
            quote_opened=Quote(None, EURUSD, 1.2000, 1.2002, 0, 0),
            strategy_id='ma-strat-001',
            ticket='asd21we4123123'
        )

    def test_set_tp(self):
        self.dalong.set_tp(10)
        self.dashort.set_tp(20)

    def test_set_sl(self):
        self.dalong.set_sl(20)
        self.dashort.set_sl(10)

    def test_set_stops(self):
        self.dalong.set_stops(10, 10)
        self.dashort.set_stops(22, 22)

    def test_tp_price(self):
        self.dalong.set_tp(10)
        assert self.dalong.tp_price == (
            self.dalong.q1.ask + 10 * self.dalong.instrument.pip
        )
        self.dalong.set_tp(22)
        assert self.dalong.tp_price == (
            self.dalong.q1.ask + 22 * self.dalong.instrument.pip
        )
        self.dashort.set_tp(33)
        self.assertEqual(
            self.dashort.tp_price,
            self.dashort.q1.bid - 33 * self.dashort.instrument.pip
        )

    def test_sl_price(self):
        self.dalong.set_sl(10)
        self.assertEqual(
            self.dalong.sl_price,
            self.dalong.q1.ask - 10 * self.dalong.instrument.pip
        )
        self.dashort.set_sl(220)
        self.assertEqual(
            self.dashort.sl_price,
            self.dashort.q1.bid + 220 * self.dashort.instrument.pip
        )

    def test_update_state(self):
        q = Quote(None, EURUSD, 1.3000, 1.3002, 0, 0)
        self.dalong.update_state(q)
        self.assertTrue(self.dalong.profit_in_pips > 0)

    def test_mark_to_market(self):
        q = Quote(None, EURUSD, 1.3000, 1.3002, 0, 0)
        self.dashort.mark_to_market(q)
        self.dashort.close()
        with self.assertRaises(FXEngineException):
            self.dashort.mark_to_market(q)

    def test_close(self):
        self.dashort.close()
        self.assertTrue(not self.dashort.is_open)
