from fx.engine.frequency import *
from unittest import TestCase


class TestFrequency(TestCase):
    def test_frequency_constants(self):
        assert FREQ_1_HOUR.seconds == 60 * 60
        try:
            FREQ_1_HOUR.seconds = 2
        except AttributeError:
            print 'Exception was expected, ok. Cannot set seconds'
        else:
            raise Exception('cant happen')
        assert FREQ_1_SEC.seconds == 1
        assert FREQ_30_MIN.seconds == 30 * 60

    def test_as_pandas_str(self):
        assert FREQ_1_HOUR.as_pandas_constant == '60T'
        assert FREQ_1_SEC.as_pandas_constant == '1S'
        self.assertRaises(NotImplementedError, lambda: FREQ_TICK.as_pandas_constant)


if __name__ == '__main__':
    import nose

    nose.run(defaultTest=__name__)