from fx.logger import fx_log


class Account(object):
    def __init__(self, balance, leverage):
        assert 1 <= leverage <= 500
        assert balance >= 5000
        super(Account, self).__init__()

        self.balance = balance
        self.equity = balance
        self.leverage = leverage
        self.margin_used = 0.0

    def __str__(self):
        return 'eq %10s (%10s) x%4s' % (
            self.equity,
            self.balance,
            self.leverage
        )

    def lock_margin(self, margin_size):
        assert margin_size > 0
        self.margin_used += margin_size

    def free_margin(self, margin_size):
        self.margin_used -= margin_size
        assert self.margin_used >= -1 # TODO ugly hack

    def update(self, profit_loss):
        assert isinstance(profit_loss, (float, int))
        assert isinstance(self.balance, (float, int))
        self.equity = self.balance + profit_loss

    def redeem_position(self, position):
        """
        Closed position gets redeemed on account
        including margin and profitloss

        @param position: closed position
        @return: None
        """
        self.free_margin(position.margin_cost)
        self.balance += position.profit_in_base_currency
        fx_log.debug("redeem:  margin:%s  profit:%s" % (
            position.margin_cost,
            position.profit_in_base_currency
        ))

    @property
    def use_of_leverage_ratio(self):
        """
        Example:
        Position of 1 mio EURUSD at 1.2000
        Exposure on the account = USD 1,200,000
        Profit and losses = 0
        Leverage authorized for the account = 1:20
        Equity = USD 100,000
        Used Margin = Exposure on the account / Leverage = USD 1,200,000 / 20
        = USD 60,000
        Use of leverage = Used Margin / Equity = 60,000 / 100,000 = 60%
        """
        return (self.margin_used / self.equity) * 100.0


class TestAccount(Account):
    def __init__(self):
        super(TestAccount, self).__init__(
            balance=1000 * 1000,
            leverage=100
        )
