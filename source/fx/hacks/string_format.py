class StringFormatExample(object):
    pass

# 3.1415926	        {:.2f}	 3.14	 2 decimal places
# 3.1415926	        {:+.2f}	 +3.14	 2 decimal places with sign
# -1                {:+.2f}	 -1.00	 2 decimal places with sign
# 2.71828	        {:.0f}	 3	 No decimal places
# 5	                {:0>2d}	 05	 Pad number with zeros (left padding, width 2)
# 5	                {:x<4d}	 5xxx	 Pad number with x's (right padding, width 4)
# 10	            {:x<4d}	 10xx	 Pad number with x's (right padding, width 4)
# 1000000	        {:,}	 1,000,000	 Number format with comma separator
# 0.25	            {:.2%}	 25.00%	 Format percentage
# 1000000000	    {:.2e}	 1.00e+09	 Exponent notation
# 13	            {:10d}	 13	 Right aligned (default, width 10)
# 13	            {:<10d}	 13	 Left aligned (width 10)
# 13	            {:^10d}	 13	 Center aligned (width 10)

# [[fill]align][sign][#][0][minimumwidth][.precision][type]

'''
        '<' - Forces the field to be left-aligned within the available
              space (This is the default.)
        '>' - Forces the field to be right-aligned within the
              available space.
        '=' - Forces the padding to be placed after the sign (if any)
              but before the digits.  This is used for printing fields
              in the form '+000000120'. This alignment option is only
              valid for numeric types.
        '^' - Forces the field to be centered within the available
              space.
'''

'''
     '+'  - indicates that a sign should be used for both
               positive as well as negative numbers
        '-'  - indicates that a sign should be used only for negative
               numbers (this is the default behavior)
        ' '  - indicates that a leading space should be used on
               positive numbers
'''

'''
        'b' - Binary. Outputs the number in base 2.
        'c' - Character. Converts the integer to the corresponding
              Unicode character before printing.
        'd' - Decimal Integer. Outputs the number in base 10.
        'o' - Octal format. Outputs the number in base 8.
        'x' - Hex format. Outputs the number in base 16, using lower-
              case letters for the digits above 9.
        'X' - Hex format. Outputs the number in base 16, using upper-
              case letters for the digits above 9.
        'n' - Number. This is the same as 'd', except that it uses the
              current locale setting to insert the appropriate
              number separator characters.
        '' (None) - the same as 'd'
'''

"{0:.4f}".format(0.1234567890) # float
"{0:.4e}".format(0.1234567890) # scientific
"{: .4e}".format(0.098765) # leave space for minus
"mem{0: 3.0f} {1:5.2f} {2: 6n}K / {3: n}".format(0)

