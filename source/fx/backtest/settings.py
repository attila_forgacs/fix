class BackTestSettings(object):

    DATA_TYPE_HDF5 = 'hdf5'
    DATA_TYPE_BINARY = 'bin'
    DATA_TYPE_RANDOM = 'random'
    DATA_TYPE_ALWAYS_UP = 'up'
    DATA_TYPE_ALWAYS_DOWN = 'down'
    DATA_TYPE_ALWAYS_FLAT = 'flat'
    DATA_TYPE_ALWAYS_SINE = 'sine'

    def __init__(self,
                 data_type=None,
                 data_source=None,
                 slippage_on_open=1,
                 slippage_on_close=1,
                 slippage_on_tp=1,
                 slippage_on_sl=1
    ):
        super(BackTestSettings, self).__init__()
        assert data_type, 'Data type must be given'
        self.data_type = data_type
        self.data_source = data_source
        self.slippage_on_open = slippage_on_open
        self.slippage_on_close = slippage_on_close
        self.slippage_on_tp = slippage_on_tp
        self.slippage_on_sl = slippage_on_sl
