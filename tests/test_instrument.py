from unittest import TestCase
from fx.engine.instrument import EURUSD


class TestInstrument(TestCase):
    def test_name(self):
        assert EURUSD.name == 'EURUSD'
        try:
            EURUSD.name = 2
        except AttributeError:
            'cant set attribute'
        else:
            raise Exception("should not set attribute")

    def test_pip(self):
        assert EURUSD.pip == 0.0001
        try:
            EURUSD.pip += 2
        except AttributeError:
            'cant set attribute'
        else:
            raise Exception("should not set attribute")