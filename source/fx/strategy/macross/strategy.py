import pandas as pd
from fx.engine.frequency import FREQ_TICK
from fx.engine.marketside import MarketSide
from fx.engine.ordertype import OrderType
import fx.strategy.abstract_strategy
from fx.strategy.macross.params import MACrossStrategyParameters


class MACrossStrategy(fx.strategy.abstract_strategy.AbstractStrategy):
    def __init__(self, broker, strategy_id, parameters):
        """
        @type strategy_id: str
        @type broker: AbstractBroker
        @type parameters: MACrossStrategyParameters
        """
        super(MACrossStrategy, self).__init__(broker, strategy_id)
        assert isinstance(parameters, MACrossStrategyParameters)
        self.parameters = parameters
        market_data = self.broker.market_data
        market_data.subscribe(parameters.instrument, FREQ_TICK, self.on_quote)
        market_data.subscribe(parameters.instrument, parameters.frequency,
                              self.on_bar)

    def calculate_indicators(self, data):
        ma_fast = pd.rolling_mean(data.close, self.parameters.fast_window)
        ma_slow = pd.rolling_mean(data.close, self.parameters.slow_window)
        ma_diff = (ma_fast - ma_slow)[-1]
        winner_direction = MarketSide.LONG if ma_diff > 0 else MarketSide.SHORT
        return ma_diff, winner_direction

    def update_portfolio(self, data):
        if len(data) >= self.parameters.slow_window:
            ma_diff, winner_direction = self.calculate_indicators(data)
            positions = self.broker.positions.of(self)
            self.trade(positions, winner_direction)

    def trade(self, positions, winner_direction):

        def enter_market_in_winning_direction():
            position = self.broker.actions.order(
                strategy=self,
                instrument=self.parameters.instrument,
                side=winner_direction,
                order_type=OrderType.MARKET,
                lot=1
            )
            position.set_stops(sl=self.parameters.sl, tp=self.parameters.tp)
            return position

        if len(positions) == 0:
            enter_market_in_winning_direction()
        else:
            if positions[0].side == winner_direction:
                pass
            else:
                self.broker.actions.close_all_position(self)
                enter_market_in_winning_direction()

    def on_bar(self, dataframe):
        super(MACrossStrategy, self).on_bar(dataframe)
        self.update_portfolio(dataframe)
