from fx.engine.frequency import Frequency
from fx.engine.instrument import Instrument
from fx.engine.parameters import StrategyParameters


class MACrossStrategyParameters(StrategyParameters):
    """"""

    def validate(self):
        assert isinstance(self.frequency, Frequency)
        assert isinstance(self.instrument, Instrument)
        assert self.slow_window > self.fast_window
        super(MACrossStrategyParameters, self).validate()

    def __init__(self, instrument=None, lot=1, sl=10, tp=40, frequency=0,
                 slow_window=10, fast_window=5):
        """
        @type frequency Frequency
        @type instrument Instrument
        """
        self.lot = lot
        self.sl = sl
        self.tp = tp
        self.slow_window = slow_window
        self.fast_window = fast_window
        self.frequency = frequency
        self.instrument = instrument
