from _dbus_bindings import _IntBase
from unittest import TestCase
from fx.backtest.settings import BackTestSettings
from fx.broker.backtest.btbr_actions import BackTestBrokerActions
from fx.broker.backtest.btbr_broker import BackTestBroker
import fx.data
from fx.data.provider import get_data_iterator
from fx.engine.frequency import FREQ_5_SEC, FREQ_TICK
from fx.backtest.backtest import BackTest
from fx.engine.instrument import EURUSD
from fx.engine.marketside import MarketSide
from fx.engine.ordertype import OrderType
from fx.engine.position import Position
from fx.strategy.abstract_strategy import AbstractStrategy


class Buy1Strat(AbstractStrategy):
    def __init__(self, broker=None, strategy_id=None):
        super(Buy1Strat, self).__init__(broker, strategy_id)
        market_data = self.broker.market_data
        market_data.subscribe(EURUSD, FREQ_TICK, self.on_quote)
        market_data.subscribe(EURUSD, FREQ_5_SEC, self.on_bar)
        self.has_traded = False

    def on_bar(self, bar):
        if not self.has_traded:
            eq1 = self.broker.account.equity
            p = self.broker.actions.order(self, EURUSD, MarketSide.LONG,
                                          OrderType.MARKET,
                                          lot=1)
            self.has_traded = 1
            assert isinstance(p, Position)
            assert isinstance(self.broker, BackTestBroker)
            assert isinstance(self.broker.actions, BackTestBrokerActions)
            self.broker.actions.close_position(p, p.q1)
            eq2 = self.broker.account.equity
            assert eq2 < eq1
            print eq1 - eq2
            # abort backtesting
            raise KeyboardInterrupt


class TestSpread(TestCase):
    def test_spread_exists(self):
        backtest_settings = BackTestSettings(
            data_type=BackTestSettings.DATA_TYPE_BINARY)
        broker = BackTestBroker.create(instruments=[EURUSD],
                                       settings=backtest_settings)
        s = Buy1Strat(broker=broker, strategy_id='macross-1')

        BackTest(
            broker=broker,
            strategies=[s],
        ).test(
            instrument=EURUSD,
            quotes=get_data_iterator(backtest_settings)
        )

