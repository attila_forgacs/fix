#!/usr/bin/env python
# -*- coding: utf-8 -*-


class OrderType(object):
    """
    Order type on market. Not every broker
    supports every order type.
    Could be: market, limit, bid/offer
    """

    MARKET = None
    CONDITIONAL = None

    def __init__(self, name):
        super(OrderType, self).__init__()
        self._name = name

    @property
    def name(self):
        return self._name

    def __str__(self):
        return 'OT:%s' % (self._name)


OrderType.MARKET = OrderType('market')
OrderType.CONDITIONAL = OrderType('limit')
