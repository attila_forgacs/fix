#!/usr/bin/env python
# -*- coding: utf-8 -*-


class MarketSide(object):
    """
    Long or Short
    """

    LONG = None
    SHORT = None

    def __init__(self, name, value):
        super(MarketSide, self).__init__()
        self._name = name
        self._value = value

    @property
    def name(self):
        return self._name

    @property
    def value(self):
        return self._name

    def __str__(self):
        return 'MS:%s' % (self._name)


MarketSide.LONG = MarketSide('LONG', +1)
MarketSide.SHORT = MarketSide('SHORT', -1)

