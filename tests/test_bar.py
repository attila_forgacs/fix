from unittest import TestCase
from fx.engine.bar import Bar


class TestBar(TestCase):
    def test_tostr(self):
        b = Bar(1, 2, 3, 4, 100, 1)
        assert b.open == 1
        assert b.high == 2
        assert b.low == 3
        assert b.close == 4
        assert b.volume == 100
        assert str(b) == 'Bar[O:1.000000 H:2.000000 L:3.000000 C:4.000000 V:100.000000]'
        assert `b` == 'Bar[O:1.000000 H:2.000000 L:3.000000 C:4.000000 V:100.000000]'
        assert unicode(b) == 'Bar[O:1.000000 H:2.000000 L:3.000000 C:4.000000 V:100.000000]'
        assert repr(b) == 'Bar[O:1.000000 H:2.000000 L:3.000000 C:4.000000 V:100.000000]'
