class SmartObject(object):
    """
    Helps creating easily debugabble classes.
    in a non-intrusive manner
    """

    def __unicode__(self):
        return str(self)

    def __repr__(self):
        return str(self)

    def __str__(self):
        raise Exception('SmartObject: please create a __str__ function')