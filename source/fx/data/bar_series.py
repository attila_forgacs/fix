from math import floor
from fx.data.provider import _random_generator
from fx.engine.frequency import Frequency
from fx.engine.instrument import EURUSD
import datetime


class BarSeries(object):
    ALL_SERIES = 'TOHLCV'

    def __init__(self,
                 instrument=None,
                 frequency=None,
                 max_bars_stored=40
    ):
        super(BarSeries, self).__init__()
        self.storage = dict(T=[], O=[], H=[], L=[], C=[], V=[], )
        self.instrument = instrument
        self.frequency = frequency
        self.max_bars_stored = max_bars_stored

    @property
    def empty(self):
        return self.count == 0

    @property
    def count(self):
        return len(self.storage['T'])

    def append(self, quote):
        '''
        @return: true if new bars has been opened
        '''
        timestamp = (
            quote.timestamp.to_pydatetime()
        )

        day_begins_at = (
            timestamp.fromordinal(timestamp.toordinal())
        )

        seconds = (
            (timestamp - day_begins_at).total_seconds()
        )

        period_start_seconds_offset = (
            floor(seconds / self.frequency.seconds)
            * self.frequency.seconds
        )

        period_start_at = (
            day_begins_at + datetime.timedelta(
                seconds=period_start_seconds_offset)
        )

        was_empty = self.empty
        if was_empty or period_start_at != self.time[-1]:
            '''seeding or new bar opened'''
            self.time.append(period_start_at)
            self.open.append(quote.bid)
            self.high.append(quote.bid)
            self.low.append(quote.bid)
            self.close.append(quote.bid)
            self.volume.append(quote.total_volume or 0)
            if not was_empty:
                if len(self.time) >= 2 * self.max_bars_stored:
                    _max = self.max_bars_stored
                    for series in self.ALL_SERIES:
                        self.storage[series] = self.storage[series][_max:]
                return True
        else:
            '''old bar updated'''
            if quote.bid > self.high[-1]:
                self.high[-1] = quote.bid
            if quote.bid < self.low[-1]:
                self.low[-1] = quote.bid
            self.close[-1] = quote.bid
            self.volume[-1] += quote.total_volume
        return False

    @property
    def time(self):
        return self.storage['T']

    @property
    def open(self):
        return self.storage['O']

    @property
    def high(self):
        return self.storage['H']

    @property
    def low(self):
        return self.storage['L']

    @property
    def close(self):
        return self.storage['C']

    @property
    def volume(self):
        return self.storage['V']


if __name__ == '__main__':

    bars = BarSeries(EURUSD, Frequency(60))
    quotes_generator = _random_generator(-4, 5, 0.0001)
    for q in quotes_generator:
        bars.append(q)

    print 'bars', bars.count
    for b in range(bars.count):
        pass

