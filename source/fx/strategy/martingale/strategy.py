import pandas as pd

from fx.strategy.abstract_strategy import *
from fx.engine.frequency import FREQ_TICK
from fx.strategy.martingale.params import MartingaleParameters


class MartingaleStrategy(AbstractStrategy):
    #
    # TODO both directions, long-short
    #

    def __init__(self, broker, strategy_id, parameters):
        super(MartingaleStrategy, self).__init__(broker, strategy_id)
        assert isinstance(parameters, MartingaleParameters)
        assert parameters.is_validated
        self.parameters = parameters
        subscribe = self.broker.market_data.subscribe
        subscribe(parameters.instrument, parameters.frequency, self.on_bar)
        subscribe(parameters.instrument, FREQ_TICK, self.on_quote)

    def on_bar(self, data):
        self.update_portfolio(data)

    def on_quote(self, quote):
        self.last_quote = quote

    def calculate_indicators(self, barseries):
        close_series = pd.Series(barseries.close)
        ma_fast = pd.rolling_mean(close_series,
                                  self.parameters.long_fast_window)
        ma_slow = pd.rolling_mean(close_series,
                                  self.parameters.long_slow_window)
        ma_diff = (ma_fast - ma_slow).tail(1)
        winner_direction = MarketSide.LONG if ma_diff > 0 else MarketSide.SHORT
        return ma_diff, winner_direction

    def get_last_position(self, positions):
        last_pos = sorted(positions,
                          lambda a, b: int(a.q1.ask - b.q1.ask))[-1]
        return last_pos

    def get_group_exit_price(self, positions):
        sum_lot = sum(p.lot for p in positions)
        sum_lot_weighted_open_prices = sum(p.lot * p.q1.ask for p in positions)
        result = sum_lot_weighted_open_prices / sum_lot
        return result

    def buy_first(self):
        P = self.parameters
        position = self.broker.actions.order(
            strategy=self,
            instrument=P.instrument,
            side=LONG,
            order_type=MARKET,
            lot=P.lot,
        )
        position.set_tp(P.grid_height)
        return position

    def buy_more(self, pos_count, positions):
        P = self.parameters
        last_position = self.get_last_position(positions)
        price_diff = abs(last_position.q1.ask - self.last_quote.ask)
        if price_diff >= P.grid_height * P.instrument.pip:
            lot = P.lot * (P.exponent ** (pos_count - P.rise_after))
            if lot < P.lot:
                lot = P.lot
            position = self.broker.actions.order(
                strategy=self,
                instrument=P.instrument,
                side=LONG,
                order_type=MARKET,
                lot=lot
            )
            group_exit_price = self.get_group_exit_price(self.my_positions)
            group_exit_price += P.long_group_tp_pips * P.instrument.pip
            for position in self.my_positions:
                position.set_tp_price(group_exit_price)
            return position

    def update_portfolio(self, barseries):

        has_enough_data = barseries.count >= self.parameters.long_slow_window

        if has_enough_data:

            ma_diff, winner_direction = self.calculate_indicators(barseries)

            positions = self.my_positions
            pos_count = len(positions)

            if ma_diff > self.parameters.ma_diff:
                if not positions:
                    self.buy_first()
                else:
                    self.buy_more(pos_count, positions)
