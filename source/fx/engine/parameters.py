class StrategyParameters(object):
    """"""

    def __init__(self):
        super(StrategyParameters, self).__init__()
        self.is_validated = False


    def validate(self):
        self.is_validated = True
        return self
