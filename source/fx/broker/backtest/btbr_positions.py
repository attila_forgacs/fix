from fx.engine.position import Position
from fx.engine.quote import Quote
from fx.logger import fx_log


class BacktestBrokerPositions(object):
    ''

    def __init__(self, broker):
        super(BacktestBrokerPositions, self).__init__()
        self.broker = broker
        self._pos = []
        self.all_positions_ever = []

    def of(self, strategy):
        return [p for p in self._pos if p.strategy_id == strategy.strategy_id]

    def add(self, strategy, position):
        assert position.strategy_id == strategy.strategy_id
        self._pos.append(position)
        self.all_positions_ever.append(position)

    def remove_all_of(self, strategy):
        self.remove(
            p for p in self._pos if p.strategy_id == strategy.strategy_id)

    def remove(self, positions):
        if isinstance(positions, Position):
            positions = [positions]
        return [
            self._pos.remove(position)
            for position in positions
        ]

    def update(self, quote):
        """ marking all positions to market on new quote """
        updated_positions = [
            p.mark_to_market(quote)
            for p in self._pos
            if quote.instrument == p.instrument
        ]
        self.apply_tp_sl(quote, updated_positions)
        return updated_positions

    def apply_tp_sl(self, quote, positions):
        closed_positions = []

        for position in positions:
            assert isinstance(position, Position)

            #
            # Helper
            #
            def apply_with_slippage(slippage, stop_name='stop'):
                slipped_quote = self.__apply_slippage(position,
                                                      quote.clone(),
                                                      slippage)
                self.broker.actions.close_position(position, slipped_quote)
                closed_positions.append(position)
                fx_log.debug('%s hit at: %s' % (stop_name, quote))


            if not position.has_stops:
                continue

            price_to_check = quote.bid if position.long else quote.ask

            if position.long:

                if position.has_tp and price_to_check >= position.tp_price:
                    apply_with_slippage(self.broker.settings.slippage_on_tp,
                                        'tp')
                elif position.has_sl and price_to_check <= position.sl_price:
                    apply_with_slippage(self.broker.settings.slippage_on_sl,
                                        'sl')

            elif position.short:

                if position.has_tp and price_to_check <= position.tp_price:
                    apply_with_slippage(self.broker.settings.slippage_on_tp,
                                        'tp')
                elif position.has_sl and price_to_check >= position.sl_price:
                    apply_with_slippage(self.broker.settings.slippage_on_sl,
                                        'sl')

        return closed_positions

    def __apply_slippage(self, position, quote, slippage):
        if position.long:
            quote.ask -= slippage * quote.instrument.pip
            quote.bid -= slippage * quote.instrument.pip
        else:
            quote.ask += slippage * quote.instrument.pip
            quote.bid += slippage * quote.instrument.pip
        return quote

    @property
    def profit_loss(self):
        """
        Assuming all positions are up to date, calculate current
        profit loss.
        """
        return sum(p.profit_in_base_currency for p in self._pos)


    def tostr(self):
        return '#%s [...]' % (len(self._pos))

    __repr__ = tostr
    __str__ = tostr

