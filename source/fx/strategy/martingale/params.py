from fx.engine.frequency import Frequency
from fx.engine.instrument import Instrument
from fx.engine.parameters import StrategyParameters


class MartingaleParameters(StrategyParameters):
    def validate(self):
        assert isinstance(self.frequency, Frequency)
        assert isinstance(self.instrument, Instrument)
        super(MartingaleParameters, self).validate()
        return self

    def __init__(self,
                 instrument=None,
                 ma_diff=0.0001,
                 lot=0.1,
                 grid_height=45,
                 exponent=1.618,
                 rise_after=3,
                 frequency=None,
                 long_group_tp_pips=15,
                 long_fast_window=12,
                 long_slow_window=32,
                 short_group_tp_pips=20,
                 short_fast_window=14,
                 short_slow_window=42,
    ):
        super(MartingaleParameters, self).__init__()
        self.lot = lot
        self.frequency = frequency
        self.instrument = instrument
        self.grid_height = grid_height
        self.exponent = exponent
        self.ma_diff = ma_diff
        self.rise_after = rise_after
        self.long_group_tp_pips = long_group_tp_pips
        self.long_fast_window = long_fast_window
        self.long_slow_window = long_slow_window
        self.short_group_tp_pips = short_group_tp_pips
        self.short_fast_window = short_fast_window
        self.short_slow_window = short_slow_window
