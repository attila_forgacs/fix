from collections import defaultdict
import pylab as pl


class Evaluator(object):
    def __init__(self):
        super(Evaluator, self).__init__()
        self.events = defaultdict(list)

    def record(self, d):
        for key, value in d.iteritems():
            self.events[key].append(value)

    def evaluate(self):
        fig = pl.figure()
        ax = fig.add_subplot(111)
        ax.plot(self.events['eq'])
        ax.plot(self.events['bl'], 'r-')
        #ax.show()
        fig.savefig('/home/attila/Desktop/bt.png')
        fig.savefig("/home/attila/Desktop/bt.eps", format="eps")


class DummyEvaluator(object):
    def __init__(self): pass

    def record(self, d): pass

    def evaluate(self): pass
