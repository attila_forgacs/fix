from unittest import TestCase
from fx.data.provider import binary_reader
from fx.engine.quote import Quote


class TestBinary_reader(TestCase):
    def test_binary_reader(self):
        r = binary_reader()
        c = 0
        for q in r:
            c += 1
            assert isinstance(q, Quote)
            if c >= 1000:
                return
