from fx.engine.position import Position
from fx.engine.quote import Quote
from fx.logger import fx_log


class BackTestBrokerActions(object):
    def __init__(self, broker):
        super(BackTestBrokerActions, self).__init__()
        self.broker = broker
        self.__pos_count = 0

    def order(self, strategy=0, instrument=0, side=0, order_type=0, lot=0):
        # TODO check if margin available
        # TODO slippage on open

        margin_cost = self.broker.calculate_margin_cost(instrument, lot)
        current_quote = self.broker.market_data.quotes[instrument][-1]

        position = Position(
            strategy_id=strategy.strategy_id,
            instrument=instrument,
            side=side,
            lot=lot,
            ticket=self.__make_ticket(),
            quote_opened=current_quote,
            margin_cost=margin_cost
        )

        self.broker.account.lock_margin(margin_cost)
        self.broker.positions.add(strategy, position)



        return position

    def __make_ticket(self):
        """
        Makes fake ticket for back testing
        """
        self.__pos_count += 1
        return "FAKE-TICKET-{0:d}".format(self.__pos_count)

    def close_all_position(self, strategy):
        """
        Convenience method for closing all positions at once
        for a given strategy.
        """
        to_close_positions = self.broker.positions.of(strategy)
        for position in to_close_positions:
            self.close_position(position, position.q2)

    def close_position(self, position, quote):
        """
        Close one position with all the bookkeeping of account refreshing
        and position list maintenance
        """
        # TODO slippage on close
        assert isinstance(position, Position)
        assert isinstance(quote, Quote)
        position.mark_to_market(quote).close()
        self.broker.account.redeem_position(position)
        self.broker.positions.remove(position)
        self.broker.account.update(self.broker.positions.profit_loss)

