from fx.broker.backtest.btbr_actions import BackTestBrokerActions
from fx.broker.backtest.btbr_positions import BacktestBrokerPositions
from fx.engine.account import TestAccount
from fx.data.marketdata import MarketData
from fx.engine.errors import FXMarginCallException
from fx.engine.frequency import FREQ_TICK


class BackTestBroker(object):
    """
    Virtual broker best suited for backtesting
    purposes.
    """

    def __init__(self, account=None, instruments=None, settings=None):
        """
        @param account:
            test account used by BackTestBroker
        @param instruments:
            list of instruments to preallocate for testing
        """
        super(BackTestBroker, self).__init__()
        assert account and settings and instruments
        self.actions = BackTestBrokerActions(self)
        self.market_data = MarketData(instruments)
        self.positions = BacktestBrokerPositions(self)
        self.account = account
        self.settings = settings

    def on_quote(self, quote, counter=0):
        instrument = quote.instrument
        self.market_data.add_quote(quote, counter)
        self.positions.update(quote)
        self.account.update(self.positions.profit_loss)
        self.market_data._notify(instrument, FREQ_TICK, quote)
        if self.account.use_of_leverage_ratio >= 100:
            raise FXMarginCallException()

    def calculate_margin_cost(self, instrument, lot):
        """
        How much margin has to be allocated at this broker
        to move 'lot' lots.
        @param instrument:
        @param lot:
        @return: float value
        """
        return (lot * instrument.lot) / self.account.leverage

    @staticmethod
    def create(instruments=None, settings=None):
        return BackTestBroker(
            account=TestAccount(),
            instruments=instruments,
            settings=settings
        )


